#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>
###! # Global profile for NextCloud deployment
###!
###! This template is used to deploy bare minimum version of NextCloud for further configuration per domain and machine. Avoid using this for configuration
###!
###! ## Federation
###!
###! The nextcloud database is expected to be federated to be accessible from multiple systems
###!
###! - [ ] Footprint how the data is stored
###!
###! ### References
###! 1. How to set up Nextcloud on NixOS https://jacobneplokh.com/how-to-setup-nextcloud-on-nixos/
###! 2. NixOS manual on nextcloud https://nixos.org/manual/nixos/stable/index.html#module-services-nextcloud

{ config, lib, pkgs,... }: lib.mkIf config.services.nextcloud.enable {
	services.nextcloud = {
		hostName = "nextcloud" + "." + config.networking.fqdn;
		# FIXME-CONTRIB(Krey): Upstream has nextcloud21 on 21.05, needs to be pushed as 21 fails deployment
		package = pkgs.nextcloud22;
		https = true;
		# FIXME-QA(Krey): implement logic that adjusts the value based on our available storage
			# FIXME-CONTRIB(Krey): This has to be above 512M (https://github.com/NixOS/nixpkgs/issues/136552)
			maxUploadSize = "512M";
		config = {
			# NOTE(Krey): Force nextcloud to use https
			overwriteProtocol = "https";

			adminuser = "admin";

			# Nextcloud PostegreSQL database configuration, recommended over using SQLite
			dbtype = "pgsql";
			dbuser = "nextcloud";
			dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
			dbname = "nextcloud";
		};
		autoUpdateApps.startAt = "05:00:00";
	};
	# DND-SECURITY(Krey): These have to have a secret handling
	## FIXME-SECURITY/QA/REPRO(Krey): Check that those are not world-readable if so we should use them for deployment
		#services.nextcloud.config.adminpass = "Heeyay";
		#services.nextcloud.config.dbpass = "Heeyay";
	# FIXME-SECURITY/QA/REPRO(Krey): These have to be generated manually atm
	services.nextcloud.config.dbpassFile = "/var/keys/nextcloud/secret_pw_database";
	services.nextcloud.config.adminpassFile = "/var/keys/nextcloud/secret_pw_admin";

	# Database
	## NOTE(Krey): Furhter research needed
	services.postgresql.enable = true;
	services.postgresql = {
		ensureDatabases = [ "nextcloud" ];
		ensureUsers = [{
				name = "nextcloud";
				ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
			}];
	};

	# Web server handling
	## Nginx
	### FIXME-QA(Krey): Add compatibility for other web servers
	services.nginx = lib.mkIf config.services.nginx.enable {
		virtualHosts = {
			"${config.services.nextcloud.hostName}" = {
				forceSSL = true;
				enableACME = true;
				sslCertificateKey = "/var/lib/acme/" + config.services.nextcloud.hostName + "/key.pem";
				sslCertificate = "/var/lib/acme/" + config.services.nextcloud.hostName + "/cert.pem";
			};
			### NOTE-FEDERATE(Krey): Just in case user somehow opens discourse.domain.tld
			"nextcloud.${config.networking.domain}" = {
				addSSL = true;
				enableACME = true;
				extraConfig = ''
					return 301 https://nextcloud.${config.networking.fqdn};
				'';
			};
		};
	};

	# ensure that postgres is running *before* running the setup
	systemd.services."nextcloud-setup" = {
		requires = ["postgresql.service"];
		after = ["postgresql.service"];
	};
}