#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>

{ config, lib,... }: lib.mkIf config.services.openssh.enable {
	services.openssh = {
		# FIXME(Krey): Add some cool banner
		banner = "FIXME-BANNER";
		useDns = true;
		# SECURITY(Krey): The only way to change this is over my dead body
		permitRootLogin = "no";
		# SECURITY(Krey): Access is only allowed using authkeys to increase the complexity needed for bruteforce and avoid annoying typing of password
		passwordAuthentication = false;
		# FIXME-SECURITY(Krey): Close the firewall once the server is configured to only allow tor-access through SSH
		openFirewall = true;
		# FIXME-SECURITY(Krey): Decide on the ciphers
		ciphers = [
			"chacha20-poly1305@openssh.com"
			"aes256-gcm@openssh.com"
			"aes128-gcm@openssh.com"
			"aes256-ctr"
			"aes192-ctr"
			"aes128-ctr"
		];
	};

	services.tor.relay.onionServices.openssh.map = [ 22 ];
}