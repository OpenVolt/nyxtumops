# REF(Krey): Unofficial wiki on setup https://nixos.wiki/wiki/Nginx
{ config, lib, ... }: lib.mkIf config.services.nginx.enable {
	# Certificates
	security.acme.certs = {
		"${config.networking.fqdn}" = {
			email = "kreyren@fsfe.org";
		};
		"${config.networking.domain}" = {
			email = "kreyren@fsfe.org";
		};
	};

	services.nginx = {
		# NOTE-HARDENING(Krey): Only allow PFS-enabled ciphers with AES256
		sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
		virtualHosts."${config.networking.fqdn}" = {
			addSSL = true;
			enableACME = true;
			root = "/var/www/" + config.networking.fqdn;
			# FIXME-QA(Krey): This has to be defined in Tor section
			# FIXME-QA(Krey): Results in permission error even with sudo used
			##add_header Onion-Location http://$\{builtins.readFile /var/lib/tor/onion/nginx/hostname};
			# FIXME-REPRO(Krey): This results in a non-fatal failure atm
			extraConfig = ''
				add_header Onion-Location http://k7m7p2opt5rvdf74fxmzifl4zuny346iu5ex2fsxkhmcjzu2r4b7auqd.onion;
			'';
		};
		# NOTE-FEDERATE(Krey): In case the user figures out a way to access the `/` while it's served by this server then redirect on hostname
		# NOTE-FEDERATE(Krey): See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Alt-Svc on the alt-svc which is used for browsers to display domain.tld while being on hostname.domain.tld
		## See `torsocks curl -v --http1.1 --user-agent "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0" https://www.facebook.com/` for reference
		##add_header alt-svc 'h2="${config.networking.fqdn}:443"; ma=86400';
		virtualHosts."${config.networking.domain}" = {
			addSSL = true;
			enableACME = true;
			extraConfig = ''
				return 301 https://${config.networking.fqdn};
			'';
		};
	};

	# FIXME(Krey): Generate HTML page with server info

	networking.firewall.allowedTCPPorts = [ 80 443 ];

	# Onion-Routing
	## Tor
	services.tor.relay.onionServices.nginx.map = [ 80 443 ];
	# FIXME-TOR(Krey): Figure out a way to set the onion url here
	# FIXME-TOR(Krey): Fatal err about dynamic attribute
	# services.nginx.virtualHosts."${config.networking.fqdn}" = {
	# 	extraConfig = ''
	# 		add_header Onion-Location http://${builtins.readFile /var/lib/tor/onion/nginx/hostname};
	# 	'';
	# };
}