#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>
###! # Global Keyclock Profile
###!
###! This file sets the bare minimum configuration to deploy keyclock

# WARN(Krey): Has too many issues to fit in our threat model

# WARN(Krey): Do not use for domain/machine-specific configuration
# DND-SECURITY(Krey): Set KeyClock to NOT accept unsecure local connections to avoid tor users messing with server, unless that's expected ?
# DND-SECURITY(Krey): WARN [org.jboss.as.txn] (ServerService Thread Pool -- 51) WFLYTX0013: The node-identifier attribute on the /subsystem=transactions is set to the default value. This is a danger for environments running multiple servers. Please make sure the attribute value is unique.
{ lib, config, ... }: lib.mkIf config.services.keycloak.enable {
	# DND-SECURITY(Krey): Handle this safely
	# DND-SECURITY(Krey): This is not stored securely
	services.keycloak.initialAdminPassword = "dh4sfd65h4s65df4h";
	services.keycloak.frontendUrl = "https://" + "keycloak" + config.networking.fqdn + "/login";

	# Database
	services.keycloak.database = {
		# DND-SECURITY(Krey): This has to be handled in a sane and reproducible way
		passwordFile = "/var/keys/keycloak/secret_pw_database";
	};

	# Certificate
	services.keycloak = {
		sslCertificate = "/var/lib/acme/" + config.networking.fqdn + "/cert.pem";
		sslCertificateKey = "/var/lib/acme/" + config.networking.fqdn + "/key.pem";
	};

	# Controller?
	# DND-SECURITY(Krey): WARNING : this package is outdated and is known to have vulnerabilities
	services.jboss.enable = true;

	# # WebServer
	# services.nginx.virtualHosts."${config.networking.fqdn}".locations."/login".extraConfig = ''
	# 	 proxy_pass http://127.0.0.1:23447;
	# '';
}