#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>

{ config, lib, pkgs, runCommand, ... }: lib.mkIf config.services.discourse.enable {
	services.discourse = {
		hostname = "discourse" + "." + config.networking.fqdn;
		sslCertificate = "/var/lib/acme/" + config.services.discourse.hostname + "/cert.pem";
		sslCertificateKey = "/var/lib/acme/" + config.services.discourse.hostname + "/key.pem";
		admin = {
			fullName = "Jacob Hrbek";
			username = "kreyren";
			# FIXME(Krey): Decide on admin mail
			email = "kreyren@fsfe.org";
			# FIXME-REPRO(Krey): This has to be handled in a reproducible way (https://github.com/NixOS/nixpkgs/issues/136972)
			passwordFile = "/var/keys/discourse/secret_pw_admin";
		};
		siteSettings = {
			# FIXME-LOW(Krey): Integrate login APIs
			# login = {
			# 	enable_github_logins = true;
			# 	github_client_id = "a2f6dfe838cb3206ce20";
			# 	github_client_secret._secret = /run/keys/discourse_github_client_secret;
			# };
		};
	};

	# Databases
	## PostgreSQL	
	services.postgresql = {
		enable = true;
		# CONTRIB(Krey): The upstream still use postgresql_11 which breaks the discourse build
		package = pkgs.postgresql_13;
	};
	# DND-SECURITY(Krey): This has to be generated from nix to be reproducible, see description for command to generate
	# FIXME-FEDARATE(Krey): Has to be configured for federating
	services.discourse = {
		# FIXME-SECURITY/QA/REPRO(Krey): Has to be generated manually atm
		secretKeyBaseFile = "/var/keys/discourse/secret_key_base";
		database = {
			name = "discourse";
			pool = 8;
			username = "discourse";
			# FIXME-SECURITY/QA/REPRO(Krey): Has to be generated manually atm
			passwordFile = "/var/keys/discourse/secret_pw_database";
			createLocally = true;
		};
	};

	# Web server
	## Nginx
	services.nginx = {
		### NOTE-FEDERATE(Krey): Just in case user somehow opens discourse.domain.tld
		virtualHosts."discourse.${config.networking.domain}" = {
			extraConfig = ''
				return 301 https://discourse.${config.networking.fqdn};
			'';
		};
	};

	# Mail
	# FIXME:(Krey): Integrate email handling
	# services.discourse.mail = {
	#
	# };
}