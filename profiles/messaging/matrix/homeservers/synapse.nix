#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>
# Reference(Krey): https://nixos.org/manual/nixos/stable/index.html#module-services-matrix-synapse
# Reference(Krey): https://web.archive.org/web/20210418105434/https://github.com/matrix-org/synapse/blob/master/docs/ACME.md
# Reference(Krey): https://matrix.org/blog/2016/02/10/advanced-synapse-setup-with-lets-encrypt
# Reference(Krey): The wiki page https://nixos.wiki/wiki/Matrix

# FIXME-SSO(Krey): Integrate this to use users from the SSO
{ config, lib, pkgs, ... }: lib.mkIf config.services.matrix-synapse.enable {
	# Database handling
	## NOTE(Krey): synapse requries postgresql database to my knowledge that is the only one supported (07/09/2021-EU)
	services.postgresql = {
		enable = true;
		# DND-SECURITY(Krey): Resolve the password in a safe way
		initialScript = pkgs.writeText "synapse-init.sql" ''
			CREATE ROLE "matrix-synapse" WITH LOGIN PASSWORD 'synapse';
			CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
				TEMPLATE template0
				LC_COLLATE = "C"
				LC_CTYPE = "C";
			'';
	};	

	# Web server
	## FIXME-QA(Krey): Add support for other webservers
	services.nginx = {
		# NOTE(Krey): Using nginx to provide TLS for synapse
		enable = true;

		virtualHosts = {
			"${config.services.matrix-synapse.server_name}" = {
				addSSL = true;
				enableACME = true;
			};

			# WARN(Krey): Avoid changes specific to `config.networking.fqdn` here, use the `nginx.nix` service definition instead
			"${config.networking.fqdn}" = {
				# forward all Matrix API calls to the synapse Matrix homeserver
				locations."/_matrix" = {
					proxyPass = "http://[::1]:8448";
				};

				locations."/.well-known/matrix/server".extraConfig =
					let
						# use 443 instead of the default 8448 port to unite the client-server and server-server port for simplicity
						server = {
							"m.server" = "${config.networking.fqdn}:443";
						};
					in ''
						add_header Content-Type application/json;
						return 200 '${builtins.toJSON server}';
					'';

				locations."/.well-known/matrix/client".extraConfig =
					let
						client = {
							"m.homeserver" =	{ "base_url" = "https://${config.networking.fqdn}"; };
							"m.identity_server" =	{ "base_url" = "https://vector.im"; };
						};
						# ACAO required to allow element-web on any URL to request this json file
					in ''
						add_header Content-Type application/json;
						add_header Access-Control-Allow-Origin *;
						return 200 '${builtins.toJSON client}';
					'';
			};
		};
	};

	# Reverse proxy
	# FIXME-QA(Krey): Implement from above to have it separated for additional logic


	# TLS/SSL handling
	# security.acme.certs = {
	# 	"synapse.${config.networking.fqdn}" = {
	# 		webroot = "/var/lib/acme/acme-challenge/";
	# 		email = "kreyren+synapse@rixotstudio.cz";
	# 	};
	# };

	
	services.matrix-synapse = rec {
		server_name = "synapse" + "." + config.networking.fqdn;
		tls_certificate_path = "/var/lib/acme/" + server_name + "/cert.pem";
		tls_private_key_path = "/var/lib/acme/" + server_name + "/key.pem";
		enable_registration = false;
		# servers = {
		# 	"matrix.org" = {
		# 		"ed25519:auto" = "Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw";
		# 	};
		# };
		# FIXME-QA/OUTSOURCE(Krey): This is/was using a default value from NixOS, we should try to outsource it
		listeners = [{
			port = 8448;
			bind_address = "";
			type = "http";
			tls = true;
			x_forwarded = false;
			resources = [
				{ names = ["client" "webclient"]; compress = true; }
				{ names = ["federation"]; compress = false; }
			];
		}];
	};

	#${services.matrix-synapse.listeners."${config.networking.fqdn}".port}
	# FIXME-QA(Krey): Use `services.matrix-synapse.listeners.*.port` to open the port
	networking.firewall.allowedTCPPorts = [
		8448
	];
}