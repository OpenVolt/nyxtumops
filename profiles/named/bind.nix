#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>
###! Global service configuration to deploy bind with bare minimum functionality that can be adjusted per-file

# FIXME(Krey): Set up serial of SOA setting by the DNS server

{ config, lib, dns, ... }:
	let
		zoneFile = dns.util.${builtins.currentSystem}.writeZone "${config.networking.fqdn}" (import (../../machines + "/${config.networking.hostName}" + /zones/main.nix) { inherit dns; inherit config; });
	in lib.mkIf config.services.bind.enable {
		# FIXME-QA(Krey): Open port set by bind
		# NOTE(Krey): We need need both TCP and UDP ports oppened for the DNS resolving to work
		networking.firewall.allowedTCPPorts = [ 53 ];
		networking.firewall.allowedUDPPorts = [ 53 ];

		services.bind = {
			zones = {
				"${config.networking.fqdn}" = {
					file = zoneFile;
					master = true;
					# FIXME-QA(Krey): Figure out how to define this adapter automatically
					masters = [ "${config.networking.interfaces.enp1s0.ipv4.addresses}" ];
				};
			};
		};
	}