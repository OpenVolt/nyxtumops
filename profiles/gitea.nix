#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>

{ config, lib, ... }: lib.mkIf config.services.gitea.enable {
	services.gitea = {
		domain = "gitea" + "." + config.networking.fqdn;
	};

	# Web server
	## Nginx
	### NOTE-FEDERATE(Krey): Just in case user somehow opens discourse.domain.tld
	services.nginx = {
		virtualHosts."gitea.${config.networking.domain}" = {
			extraConfig = ''
				return 301 https://${config.services.gitea.domain}};
			'';
		};

		virtualHosts."${config.services.gitea.domain}" = {
			addSSL = true;
			enableACME = true;
		};
	};
}