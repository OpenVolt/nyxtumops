#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>

{ config, lib,... }: lib.mkIf config.services.ntp.enable {
	services.ntp = {
		servers = [
			"ntp1.snopyta.org"
			"ntp2.snopyta.org"
			"ntp3.snopyta.org"
		];
	};
}