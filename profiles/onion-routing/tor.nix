#@ Copyright (C) Jacob Hrbek <kreyren@fsfe.org> 08/09/2021-EU released under OpenVolt license <https://git.dotya.ml/OpenVolt/OpenVolt/src/branch/central/LICENSE.md>
# REF(Krey): Exit policy https://gitlab.torproject.org/legacy/trac/-/wikis/doc/ReducedExitPolicy
{ config, lib,... }: lib.mkIf config.services.tor.relay.enable {
	networking.firewall.allowedTCPPorts = [ 36821 ];

	services = {
		tor = {
			relay = {
				role = "exit";
			};
			client = {
				dns.enable = true;
			};
			settings = {
				ORPort = "auto";
			};
		};
	};
}

