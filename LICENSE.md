=== START OF LICENSE ===

Copyright (C) 23/06/2021-EU by Jacob Hrbek <kreyren@rixotstudio.cz>

This license currently mimics the terms declared by the GPLv3 license <https://www.gnu.org/licenses/gpl-3.0-standalone.html> while reserving permission to re-license

=== END OF LICENSE ===

Stub license until RXT finishes it's own