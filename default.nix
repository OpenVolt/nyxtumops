# Gateway in the project configuration, used for global configuration

{ config, lib, pkgs, ... }:
let
	# Global variables

	## Get hostname to be later set as `networking.hostName` to make it available for logic
	### FIXME-QA(Krey): Duplicate code
	hostname = if((builtins.getEnv "HOSTNAME") == "nixos")
		then throw "The use of hostname 'nixos' is a configuration error this deployment method requires a unique domain to apply the configuration"
		else if((builtins.getEnv "HOSTNAME") == "")
			then throw "Environmental variable 'HOSTNAME' is blank, unable to detect the system hostname"
			else builtins.getEnv "HOSTNAME";
		
		### NOTE(Krey): Uses `lib.splitString` to remove trailing new-line
		#  if((lib.head (lib.splitString "\n" (builtins.readFile /etc/hostname))) != "" && (lib.head (lib.splitString "\n" (builtins.readFile /etc/hostname))) !)
		# then lib.head (lib.splitString "\n" (builtins.readFile /etc/hostname))
		# else throw "Unable to find sane hostname";

	## Get domain to be later set as `networking.domain` to make it available for logic
	### DND(Krey): Figure out how to get this value from the system
	domain = if(true)
		then "openvolt.tk"
		else throw "Unable to find sane domain";

	## NOTE(Krey): Handling of https://github.com/NixOS/nixpkgs/issues/128286
	## CONTRIB(Krey): Integrate this in nixOS
	nixFilesIn = path:
		let
			names = lib.filter (lib.hasSuffix ".nix") (lib.attrNames (builtins.readDir path));
		in
			map (x: path + "/${x}") names;
in {
	# Global import of 3rd party modules
	_module.args = {
		# Module used to maintain the zonefiles from nix https://github.com/kirelagin/dns.nix
		# FIXME-CI(Krey): This has to be kept up-to-date
		dns = import (pkgs.fetchFromGitHub {
			owner = "kirelagin";
			repo = "dns.nix";
			rev = "v1.1.0";
			sha256 = "0zvg92fjrfmdylk8ic3b2srsrqc8ii94a1ir0v5sknjyxvy5f3rf";
		});
	};

	networking.hostName = hostname;
	networking.domain = domain;

	imports = [
		#./hardware-configuration.nix

		(./domains + "/${domain}")
	]
		++ nixFilesIn ./profiles
		++ nixFilesIn ./profiles/cloud
		++ nixFilesIn ./profiles/forum
		++ nixFilesIn ./profiles/identity-servers
		++ nixFilesIn ./profiles/messaging
		++ nixFilesIn ./profiles/messaging/matrix
		++ nixFilesIn ./profiles/messaging/matrix/homeservers
		++ nixFilesIn ./profiles/messaging/matrix/webrtc
		++ nixFilesIn ./profiles/named
		++ nixFilesIn ./profiles/onion-routing
		++ nixFilesIn ./profiles/webservers
		++ nixFilesIn ./users;

	# Global services
	networking.firewall.enable = true;

	# Global system configuration
	system = {
		autoUpgrade = {
			enable = true;
			channel = "https://channels.nixos.org/nixos-21.05";
			allowReboot = true;
		};
	};

	# Global Nix configuration
	nix = {
		autoOptimiseStore = true;
		# GarbageCollection
		gc = {
			automatic = true;
			dates = "weekly";
			# NOTE-PRIVACY(Krey): To make target system harder to fingerprint
			randomizedDelaySec = "45min";
		};
		# TODO-SECURITY(Krey): Make group nix so that user can't just make it to gain access
		# allowedUsers = [
		# 	"@nix"
		# ];
	};
}
