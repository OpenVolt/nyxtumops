{ config, ... }: {
	# DND-SECURITY(Krey): Figure out how to handle the password
	# TODO(Krey): Integrate fingerprint scanners for auth
	users.users.kreyren = {
		isNormalUser = true;
		extraGroups = [
			"wheel"
		];
		# SSH access
		openssh.authorizedKeys.keys = [
			"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOzh6FRxWUemwVeIDsr681fgJ2Q2qCnwJbvFe4xD15ve kreyren@fsfe.org"
		];
	};

	nix.trustedUsers = [ "kreyren" ];
	# FIXME-DUP(Krey): Duplicate code above
	nix.sshServe.keys = [
		"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOzh6FRxWUemwVeIDsr681fgJ2Q2qCnwJbvFe4xD15ve kreyren@fsfe.org"
	];
}