Directory set to define global users for all domains and systems

### Example

```nix
{
	users.users.kreyren = {
		isNormalUser = true;
		extraGroups = [
			"wheel"
		];
		# SSH access
		openssh.authorizedKeys.keys = [
			"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOzh6FRxWUemwVeIDsr681fgJ2Q2qCnwJbvFe4xD15ve kreyren@fsfe.org"
		];
	};
}
```