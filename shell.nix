#!/usr/bin/env nix-shell
# NixOS shell configuration to bootstrap the required dependencies

with import <nixpkgs> {};
stdenv.mkDerivation {
	name = "OpenVolt";
	buildInputs = [
		# NOTE(Krey): Cargo make is used to provide an interface to interact with the repository
		pkgs.buildPackages.cargo-make
	];
}