# NyxtumOps

NixOS Public System Management ("NPSM") available for public review and contribution gh git for action-based deployment from git repository hosting platform e.g. Gitea

### Terminology

Global = Definition applied for all systems using this setup no matter the domain nor hostname

Domain-wide = Definitions applied for systems in specific domian `domains/<domain.tld>`

System-specific = For device specific configuration determined by hostname

### References

NixOS Manual: https://nixos.org/manual/nixos/stable/index.html
NixOS Options: https://search.nixos.org/option