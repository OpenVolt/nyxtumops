# File dedicated to set domain-wide directives

{ config, lib, ... }:
let 
	# FIXME(Krey): Figure out how to source this globally
	nixFilesIn = path:
	let
		names = lib.filter (lib.hasSuffix ".nix") (lib.attrNames (builtins.readDir path));
	in
		map (x: path + "/${x}") names;
in {
	environment = {
		interactiveShellInit = "set PATH $PATH $HOME/.local/bin";
	};

	# Domain-wide services
	services.ntp.enable = true;
	services.clamav.daemon.enable = true;
	services.clamav.updater.enable = true;
	services.tor.enable = true;
	networking.firewall.enable = true;

	# boot.loader.grub = {
	# 	enable = true;
	# 	version = 2;
	# 	enableCryptodisk = true;
	# 	# FIXME-ORG(Krey): Ban EFI org-wide
	# 	efiSupport = false;
	# };
	# boot.initrd.luks.devices = {
	# 	root = {
	# 		preLVM = true;
	# 	};
	# };

	# NOTE(Krey): Using stable since this is production environment
	system.stateVersion = "21.05";

	# Set your time zone
	time.timeZone = "Europe/Prague";

	# FIXME-QA(Krey): Consult with NixOS devs if this is the best way to do this
	# NOTE(Krey): Set to allow rootless deployment -- https://nixops.readthedocs.io/en/latest/guides/deploy-without-root.html
	security.sudo.wheelNeedsPassword = false;

	imports = [
		# DND(Krey): Figure out why is this causing an infinite recursion
		./machines/finnaboo
		#(./machines/ + config.networking.hostName)
		#(./machines + "/${config.networking.hostName}")
		#"./machines/${config.networking.hostName}"
	]
		++ nixFilesIn ./users
		++ nixFilesIn ./profiles
		++ nixFilesIn ./profiles/forum
		++ nixFilesIn ./profiles/www;
}