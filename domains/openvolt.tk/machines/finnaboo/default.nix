{ config, pkgs, lib, ... }:
	let
		# FIXME(Krey): Figure out how to source this globally
		nixFilesIn = path:
		let
			names = lib.filter (lib.hasSuffix ".nix") (lib.attrNames (builtins.readDir path));
		in
			map (x: path + "/${x}") names;
	in {
		imports = [
			# NOTE(Krey): Include the auto-generated hardware configuration
			./hardware-configuration.nix
		];

		# Xen
			## DECIDE(Krey): Decide whether we want Xen on this system
			## BUG(Krey): Breaks the boot on leonid and i didn't tested it on finaboo yet
			virtualisation.xen.enable = false;
			#boot.kernelModules = [ "kvm-intel" ];
			#virtualisation.libvirtd.enable = true;

		# NOTE(Krey): Disable any non-FLOSS components
			nixpkgs.config.allowUnfree = false;
			hardware.enableAllFirmware = false;

		# Networking
			networking.networkmanager = {
				enable = true;
			};

		# Sound
			## NOTE(Krey): This is a server so there shoudn't be any need for sound other then scaring hetzner's sysadmins
			sound.enable = false;
			hardware.pulseaudio.enable = false;

		# Environment
			# environment.systemPackages = with pkgs; [
			# 	wineWowPackages.staging

			# 	(winetricks.override { wine = wineWowPackages.staging; })
			# ];

		# Fonts
		fonts.fonts = with pkgs; [
			# NOTE(Krey): Fonts with liqurature support
			fira-code
			fira-code-symbols
		];

		# Local services
			## TODO(Krey): Decide on Lokinet
			services.loki.enable = false;
			services.tor.relay.enable = true;
			services.tor.client.enable = true;
			services.openssh.enable = true;
			# FIXME(Krey): Needs more work to handle the zonefiles from nix
			# NOTE-HOTFIX(Krey): Currently we are setting the DNS record on the domain provider
			services.bind.enable = false;
			# FIXME(Krey): Needs declaration in the zonefile of DNS
			# FIXME(Krey): Needs more work
			services.matrix-synapse.enable = false;
				services.coturn.enable = false;
			# BUG(Krey): Discourse fails to build https://github.com/NixOS/nixpkgs/issues/136547
			services.discourse.enable = true;
			services.nginx.enable = true;
			# FIXME(Krey): Add email handling (postfix + dovecot)
			services.nextcloud.enable = true;
				services.nextcloud.autoUpdateApps.enable = true;
			services.grafana.enable = false;
			services.gitea.enable = false;
			services.mastodon.enable = false;
			#services.invidious.enable = false;
			#services.nitter.enable = false;
			#services.bibliogram.enable = false;
			#services.libreddit.enable = true;
			services.searx.enable = false;
			# TODO(Krey): IRC
			# TODO(Krey): Status page

		# Boot configuration
			## FIXME-CRYPT(Krey): This server should be encrypted
			boot.loader.grub.enableCryptodisk = false;
			#boot.initrd.luks.devices.root.preLVM = lib.mkForce(false);
			boot.loader.grub.devices = [ "/dev/sda" ];

		# Tor configuration
			services.tor.settings.Nickname = "HEEEYAAAAY";
			## NOTE(Krey): Set bandwidth limit
			services.tor.settings.RelayBandwidthRate = "2621440"; # 20 mbit/s

		# Filesystems on the system
		fileSystems = {
			# The root filesystem
			"/" = {
				label = "NIXOS";
				device = "/dev/sda1";
				fsType = "btrfs";
				# NOTE(Krey): Not supported for btrfs
					autoResize = false;
				autoFormat = true;
			};
		};
	}