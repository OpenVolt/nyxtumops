# File integrating https://github.com/kirelagin/dns.nix

{ dns, config }: {
	SOA = {
			nameServer = ("ns." + config.networking.fqdn);
			# FIXME(Krey): Decide on the email for hostmaster
			adminEmail = "kreyren@fsfe.org";
			# FIXME(Krey): Generate this YYMMDDHH
			serial = 10;
		};

	NS = [
		# WARN(Krey): The dot is VERY important, i found that the hard way
		("ns." + config.networking.fqdn + ".")
	];

	# FIXME-QA(Krey): The interface 'enp1s0' is hard-codded which is unwanted
	# FIXME(Krey): The use of `networking.interfaces.enp1s0.ipv4.addresses` causes anonymous function error
	A = [
		#{ address = "${networking.interfaces.enp1s0.ipv4.addresses}"; }
		{ address = "65.21.145.149"; }
	];

	# AAAA = [
	# 	#{ address = "${networking.interfaces.enp1s0.ipv6.addresses}"; }
	# 	{ address = "2a01:4f9:c010:4b8b::/64"; }
	# ];

	 subdomains = {
		ns.A = [ "65.21.145.149" ];
	 };
}

