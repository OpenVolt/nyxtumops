{ ... }: {
	# NOTE(Krey): Configuration required to set up SSL cert using ACME
	security.acme.email = "kreyren@fsfe.org";
	security.acme.acceptTerms = true;
}