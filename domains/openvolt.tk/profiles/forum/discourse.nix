{ config, lib, pkgs, ... }: lib.mkIf config.services.discourse.enable {
	services.discourse.siteSettings = {
		# NOTE(Krey): Available settings are available on https://github.com/discourse/discourse/blob/main/config/site_settings.yml
		required = {
			title = "OpenVolt";
			site_description = "Restoring control to your hands!";
		};
	};

	# Plugins
	## FIXME(Krey): Cherrypick wanted plugins
	# services.discourse.plugins = {
	#
	# }
}