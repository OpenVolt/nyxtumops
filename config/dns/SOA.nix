# Global configuration of SOA for DNS server if relevant option is set

# DND-PSEUDO(Krey): Pseudo code to determine the integration

# FIXME-QA(Krey): Set this to apply only if either of supported name servers are enabled
{ lib, config }: lib.mkIf true {
	SOA = {
		# DND(Krey): Integrate `config.networking.fqdn`
		nameServer = if(config.authority.nameserver != "")
			then "${config.authority.nameserver}"
			else throw "Unable to find a sane authority nameserver";
		# FIXME(Krey): Import this domain-wide
		adminEmail = if(config.hostmaster.email != "")
			then "${config.hostmaster.email}"
			else throw "Unable to find sane hostmaster email address for the SOA record";
		# FIXME(Krey): Generate this YYMMDDHH
		serial = 0;
	};
}